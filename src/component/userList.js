import React from "react";

function UserList(props){
    return(
        <tr>
            <td>{props.item.userId}</td>
            <td>{props.item.name}</td>
            <td>{props.item.address}</td>
            <td>{props.item.phone}</td>
        </tr>
    )
}
export default UserList

