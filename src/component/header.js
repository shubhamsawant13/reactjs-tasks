import React from 'react';

import '../style.css';
import logo from './logo.png'

function Header(){
        return(
            <div className="header">
                <div className='logo'>
                    <img src={logo} alt="test"></img>
                </div>
                <div className='link'>
                    <ul>
                        <li><a  href='#'>HOME</a></li>
                        <li><a  href='#'>ABOUT</a></li>
                        <li><a  href='#'>GALLERY</a></li>
                        <li><a  href='#'>CONTACT</a></li>
                    </ul>
                </div>
                
            </div>
        );
    }

export default Header