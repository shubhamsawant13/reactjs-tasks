import React from 'react';

import Header from './header'
import Sidebar from './sidebar'
import Mainbar from './mainbar'

function App (){
        return(
            <div>
                <Header/>
                <Sidebar/>
                <Mainbar/>
                
            </div>
        );
    }

export default App