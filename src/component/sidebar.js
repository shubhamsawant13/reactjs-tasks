import React from 'react';
import UserData from './userData';

import '../style.css'

function Sidebar (){
    const length = UserData.length;
        return(
            <div className="sidebar">
                    
                    <p className='addCount'><b>Count: {length}</b></p>
            </div>
        );
    }

export default Sidebar