import React from 'react';
import UserData from './userData';
import UserList from './userList';

import '../style.css'

function Mainbar(){
    const [name, setName] = React.useState("");
    const [address, setAddress] = React.useState("");
    const [phone, setPhone] = React.useState("");

    const getList = UserData.map((item)=>(
        <UserList key={item.userId} item={item} />
    ));

    const addUserList = (e)=>{
        e.preventDefault();
        alert("Following Data Has Been To The List...! \n\nName: "+name+"\nAddress: "+address+"\nPhone: "+phone);
    }

    return(
        <div className="Mainbar">
            <div className='addNew' >
                <button >+ADD NEW USER</button>
                <div className="inputs">
                    <form className="form" onSubmit={addUserList}>
                        Name: <input type="text" name="name" onChange={e => setName(e.target.value)} required></input>
                        Address: <input type="text" name="address" onChange={e => setAddress(e.target.value)} required></input>
                        Phone: <input type="text" name="phone" onChange={e => setPhone(e.target.value)} required></input>
                        <input type="submit" name="submit" value="SUBMIT"></input>
                    </form>
                </div>
            </div>
            <div className='viewUser'>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>ADDRESS</th>
                            <th>MOBILE</th>
                        </tr>
                    </thead>
                    <tbody>
                        {getList}
                    </tbody>
                </table>
            </div>
            
        </div>
    );
}

export default Mainbar

       